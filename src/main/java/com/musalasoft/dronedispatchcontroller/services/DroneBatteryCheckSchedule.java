package com.musalasoft.dronedispatchcontroller.services;

import com.musalasoft.dronedispatchcontroller.data.models.drone.Drone;
import com.musalasoft.dronedispatchcontroller.data.models.drone.DroneRepository;
import com.musalasoft.dronedispatchcontroller.data.models.eventlog.BatteryEventLog;
import com.musalasoft.dronedispatchcontroller.data.models.eventlog.BatteryEventLogRepository;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Schedule with intervals of one minute
 * to keep logs of battery capacity in database
 */
@Component
public class DroneBatteryCheckSchedule {
    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private BatteryEventLogRepository eventLogRepository;

    @Autowired
    private Logger logger;

    @Scheduled(cron = "0 */1 * * * *")
    public void schedule() {
        logger.info("Start of cron to log battery events");
        List<Drone> droneList = droneRepository.findAll();

        for (Drone drone : droneList) {
            new Thread(() -> {
                BatteryEventLog batteryEventLog = new BatteryEventLog();

                batteryEventLog.setBatteryCapacity(drone.getBatteryCapacity());
                batteryEventLog.setDroneId(drone.getId());
                batteryEventLog.setDate(new Date());

                eventLogRepository.save(batteryEventLog);
            }).start();
        }


        logger.info("Stop of cron to log battery events");
    }
}
