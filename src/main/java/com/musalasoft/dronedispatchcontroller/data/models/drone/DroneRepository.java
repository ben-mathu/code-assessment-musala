package com.musalasoft.dronedispatchcontroller.data.models.drone;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface DroneRepository extends JpaRepository<Drone, Long> {
    List<Drone> findByState(State idle);
}
