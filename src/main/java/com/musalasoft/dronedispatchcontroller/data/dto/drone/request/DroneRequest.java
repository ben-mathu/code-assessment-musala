package com.musalasoft.dronedispatchcontroller.data.dto.drone.request;

/**
 * Defines a Drone DTO that hold requested data
 * @see DroneDto
 */
public class DroneRequest {
    private DroneDto drone;

    public DroneDto getDrone() {
        return drone;
    }

    public void setDrone(DroneDto drone) {
        this.drone = drone;
    }
}
