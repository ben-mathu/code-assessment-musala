package com.musalasoft.dronedispatchcontroller.data.dto.medication.request;

/**
 * Medication DTO
 * @see MedicationDto
 */
public class MedicationRequest {
    private MedicationDto medication;

    public MedicationDto getMedicationDto() {
        return medication;
    }

    public void setMedication(MedicationDto medication) {
        this.medication = medication;
    }
}
