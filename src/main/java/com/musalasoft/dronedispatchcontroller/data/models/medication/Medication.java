package com.musalasoft.dronedispatchcontroller.data.models.medication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.musalasoft.dronedispatchcontroller.data.models.drone.Drone;

import javax.persistence.*;

/**
 * Medication model, defines a model in database
 */
@Entity
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private int weight;
    private String code;
    @Column(name = "image_url")
    private String imageUrl;
    @JsonIgnore
    @ManyToOne(optional = false)
    @JoinColumn(name = "drone_id", referencedColumnName = "id")
    private Drone drone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }
}
