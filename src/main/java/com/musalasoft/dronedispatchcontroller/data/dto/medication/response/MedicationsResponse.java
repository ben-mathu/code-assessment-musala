package com.musalasoft.dronedispatchcontroller.data.dto.medication.response;

import com.musalasoft.dronedispatchcontroller.data.models.drone.Drone;

/**
 * List of medication response DTO
 * Contains drone object with list of medications
 * @see Drone
 */
public class MedicationsResponse {
    private Drone drone;

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }
}
