package com.musalasoft.dronedispatchcontroller.controllers;

import com.musalasoft.dronedispatchcontroller.data.dto.drone.request.DroneDto;
import com.musalasoft.dronedispatchcontroller.data.dto.drone.response.DroneResponse;
import com.musalasoft.dronedispatchcontroller.data.dto.medication.request.MedicationDto;
import com.musalasoft.dronedispatchcontroller.data.dto.medication.response.MedicationResponse;
import com.musalasoft.dronedispatchcontroller.data.dto.message.MessageResponse;
import com.musalasoft.dronedispatchcontroller.data.models.drone.Drone;
import com.musalasoft.dronedispatchcontroller.data.models.drone.DroneRepository;
import com.musalasoft.dronedispatchcontroller.data.models.drone.State;
import com.musalasoft.dronedispatchcontroller.data.models.medication.Medication;
import com.musalasoft.dronedispatchcontroller.data.models.medication.MedicationRepository;
import com.musalasoft.dronedispatchcontroller.services.DroneService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DroneControllerTest {

    @Autowired
    private DroneService droneService;
    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private MedicationRepository medicationRepository;

    private final String validModel = "Heavyweight";
    private final int weightLimit = 500;
    private final String serialNumber = "920392";
    private final int batteryCapacity = 100;
    private final String medicationCode = "898293";
    private final String medicationName = "Clear Skies";
    private final String medicationImageUrl = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fapi.time.com%2Fwp-content%2Fuploads%2F2020%2F03%2Fdrugs-shortage-us.jpg&f=1&nofb=1&ipt=e200629434603191ea133cee83d581a948b1b0e46a1ab5ee1f95c2768a2109e9&ipo=images";

    private Drone drone;
    private MedicationDto dto;

    @Before
    public void setUp() throws Exception {
        drone = new Drone();
        drone.setState(State.LOADING);
        drone.setModel(validModel);
        drone.setWeightLimit(weightLimit);
        drone.setSerialNumber(serialNumber);
        drone.setBatteryCapacity(batteryCapacity);

        dto = new MedicationDto();
        dto.setWeight(100);
        dto.setCode(medicationCode);
        dto.setName(medicationName);
        dto.setImageUrl(medicationImageUrl);
    }

    @Test
    public void registerDrone_InvalidModel_SendMessage() {
        DroneDto droneDto = new DroneDto();
        droneDto.setModel("SomeWeight");
        droneDto.setSerialNumber(serialNumber);
        ResponseEntity<Object> response = droneService.saveDrone(droneDto);

        MessageResponse messageResponse = (MessageResponse) response.getBody();
        assertNotNull(messageResponse);
        assertEquals("The model specified is not allowed", messageResponse.getMessage());
    }

    @Test
    public void registerDrone_ValidParams_SendObjectCreated() {
        DroneDto droneDto = new DroneDto();
        droneDto.setModel(validModel);
        droneDto.setSerialNumber(serialNumber);
        ResponseEntity<Object> response = droneService.saveDrone(droneDto);

        DroneResponse responseBody = (DroneResponse) response.getBody();

        assertNotNull(responseBody);
    }

    @Test
    public void loadMedication_InvalidDroneId_ShowMessage() {
        Long droneId = 1000L;

        ResponseEntity<Object> response = droneService.loadMedication(dto, droneId);
        MessageResponse messageResponse = (MessageResponse) response.getBody();

        assertNotNull(messageResponse);
        assertEquals("Drone with ID: 1000 not found", messageResponse.getMessage());
    }

    @Test
    public void loadMedication_DroneInLoadingState_ShowMessage() {
        droneRepository.save(drone);
        Long droneId = drone.getId();

        ResponseEntity<Object> response = droneService.loadMedication(dto, droneId);
        MessageResponse messageResponse = (MessageResponse) response.getBody();

        assertNotNull(messageResponse);
        assertEquals(String.format("Already loading drone ID %d", drone.getId()), messageResponse.getMessage());
    }

    @Test
    public void loadMedication_DroneBatteryCapacityLessThan25_ShowMessage() {
        drone.setState(State.IDLE);
        drone.setBatteryCapacity(23);
        drone = droneRepository.save(drone);
        Long droneId = drone.getId();

        ResponseEntity<Object> response = droneService.loadMedication(dto, droneId);
        MessageResponse messageResponse = (MessageResponse) response.getBody();

        assertNotNull(messageResponse);
        assertEquals(String.format("Drone ID %d battery capacity is below 25", drone.getId()), messageResponse.getMessage());
    }

    @Test
    public void loadMedication_InvalidMedicationValues_ShowMessage() {
        Long droneId = 1000L;

        ResponseEntity<Object> response = droneService.loadMedication(new MedicationDto(), droneId);
        MessageResponse messageResponse = (MessageResponse) response.getBody();

        assertNotNull(messageResponse);
        assertEquals("Missing arguments", messageResponse.getMessage());
    }

    @Test
    public void loadMedication_DroneLoadedWithOverWeightMedication_ShowMessage() {
        drone.setState(State.IDLE);
        drone.setBatteryCapacity(50);
        drone = droneRepository.save(drone);
        dto.setWeight(weightLimit+100);
        Long droneId = drone.getId();

        ResponseEntity<Object> response = droneService.loadMedication(dto, droneId);
        MessageResponse messageResponse = (MessageResponse) response.getBody();

        assertNotNull(messageResponse);
        assertEquals(String.format("Weight limit exceeded"), messageResponse.getMessage());
    }

    @Test
    public void loadMedication_DroneLoadedWithCumulativeOverWeightMedication_ShowMessage() {
        drone.setState(State.IDLE);
        drone.setBatteryCapacity(50);
        drone = droneRepository.save(drone);

        Medication medication = new Medication();
        medication.setDrone(drone);
        medication.setName(medicationName);
        medication.setCode(medicationCode);
        medication.setWeight(300);
        medication.setImageUrl(medicationImageUrl);

        medicationRepository.save(medication);

        dto.setWeight(300);
        Long droneId = drone.getId();

        ResponseEntity<Object> response = droneService.loadMedication(dto, droneId);
        MessageResponse messageResponse = (MessageResponse) response.getBody();

        assertNotNull(messageResponse);
        assertEquals(String.format("Weight limit exceeded"), messageResponse.getMessage());
    }

    @Test
    public void loadMedication_ValidMedication_SendJson() {
        drone.setState(State.IDLE);
        drone.setBatteryCapacity(50);
        drone = droneRepository.save(drone);

        Medication medication = new Medication();
        medication.setDrone(drone);
        medication.setName(medicationName);
        medication.setCode(medicationCode);
        medication.setWeight(300);
        medication.setImageUrl(medicationImageUrl);

        medicationRepository.save(medication);

        Long droneId = drone.getId();

        ResponseEntity<Object> response = droneService.loadMedication(dto, droneId);
        MedicationResponse medicationResponse = (MedicationResponse) response.getBody();

        assertNotNull(medicationResponse);
    }

    @After
    public void tearDown() {
        medicationRepository.deleteAll();
        droneRepository.deleteAll();
    }
}