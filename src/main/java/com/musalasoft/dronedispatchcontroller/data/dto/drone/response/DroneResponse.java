package com.musalasoft.dronedispatchcontroller.data.dto.drone.response;

import com.musalasoft.dronedispatchcontroller.data.models.drone.Drone;

/**
 * Response DTO
 */
public class DroneResponse {
    private Drone drone;

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }
}
