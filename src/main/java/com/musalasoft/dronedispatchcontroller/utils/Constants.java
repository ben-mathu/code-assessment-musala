package com.musalasoft.dronedispatchcontroller.utils;

import java.util.LinkedHashMap;
import java.util.Map;

public class Constants {
    public final static LinkedHashMap<String, Integer> modelList =
            new LinkedHashMap<>(Map.of("Lightweight", 125, "Middleweight", 250, "Cruiserweight", 375, "Heavyweight", 500));
}
