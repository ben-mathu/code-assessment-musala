package com.musalasoft.dronedispatchcontroller.data.models.drone;

/**
 * Defines the state of a drone
 */
public enum State {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
