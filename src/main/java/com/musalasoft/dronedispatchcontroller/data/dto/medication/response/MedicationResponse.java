package com.musalasoft.dronedispatchcontroller.data.dto.medication.response;

import com.musalasoft.dronedispatchcontroller.data.models.medication.Medication;

/**
 * Medication response DTO
 * @see Medication
 */
public class MedicationResponse {
    private Medication medication;

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }
}
