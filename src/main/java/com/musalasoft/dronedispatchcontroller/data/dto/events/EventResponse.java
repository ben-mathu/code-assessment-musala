package com.musalasoft.dronedispatchcontroller.data.dto.events;

import com.musalasoft.dronedispatchcontroller.data.models.eventlog.BatteryEventLog;

import java.util.List;

/**
 * Events response DTO
 */
public class EventResponse {
    private List<BatteryEventLog> eventLogList;

    public List<BatteryEventLog> getEventLogList() {
        return eventLogList;
    }

    public void setEventLogList(List<BatteryEventLog> eventLogList) {
        this.eventLogList = eventLogList;
    }
}
