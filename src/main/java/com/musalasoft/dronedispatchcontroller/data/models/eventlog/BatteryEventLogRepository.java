package com.musalasoft.dronedispatchcontroller.data.models.eventlog;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BatteryEventLogRepository extends JpaRepository<BatteryEventLog, Long> {
    List<BatteryEventLog> findByDroneId(Long droneId);
}
