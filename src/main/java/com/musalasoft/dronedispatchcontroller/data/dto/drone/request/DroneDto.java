package com.musalasoft.dronedispatchcontroller.data.dto.drone.request;

public class DroneDto {
    private String serialNumber;
    private String model;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
