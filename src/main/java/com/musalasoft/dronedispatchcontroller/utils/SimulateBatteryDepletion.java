package com.musalasoft.dronedispatchcontroller.utils;

import com.musalasoft.dronedispatchcontroller.data.models.drone.Drone;
import com.musalasoft.dronedispatchcontroller.data.models.drone.DroneRepository;
import com.musalasoft.dronedispatchcontroller.data.models.drone.State;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Schedule with intervals of three minutes
 * to simulate battery depletion, updates the database
 * <p>
 * Battery is charged when drone state is IDLE, LOADING or
 * LOADED
 * <p>
 * Battery is depleted when drone state is RETURNING, DELIVERING or
 * DELIVERED
 */
@Component
public class SimulateBatteryDepletion {

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private Logger logger;

    @Scheduled(cron = "0 */3 * * * *")
    public void scheduleDelivery() {
        List<Drone> droneList = droneRepository.findAll();

        for (Drone drone : droneList) {
            new Thread(() -> {
                if ((drone.getState() == State.IDLE || drone.getState() == State.LOADING
                        || drone.getState() == State.LOADED) && drone.getBatteryCapacity() < 100) {
                    drone.setBatteryCapacity(drone.getBatteryCapacity() + 15);
                    logger.info(String.format("Charging drone %d. Capacity: %d",
                            drone.getId(), drone.getBatteryCapacity()));
                }

                if ((drone.getState() == State.RETURNING || drone.getState() == State.DELIVERING
                        || drone.getState() == State.DELIVERED) && drone.getBatteryCapacity() > 0) {
                    drone.setBatteryCapacity(drone.getBatteryCapacity() - 15);
                    logger.info(String.format("Discharging drone %d. Capacity: %d",
                            drone.getId(), drone.getBatteryCapacity()));
                }

                droneRepository.save(drone);
            }).start();
        }
    }
}
