package com.musalasoft.dronedispatchcontroller.utils;

import com.musalasoft.dronedispatchcontroller.data.models.drone.Drone;
import com.musalasoft.dronedispatchcontroller.data.models.drone.DroneRepository;
import com.musalasoft.dronedispatchcontroller.data.models.drone.State;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Schedule with intervals of five minutes
 * to simulate various transition of drone state
 * in database
 */
@Component
public class SimulateMedicationDeliver {

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private Logger logger;

    @Scheduled(cron = "0 */5 * * * *")
    public void scheduleDelivery() {
        List<Drone> droneList = droneRepository.findAll();

        for (Drone drone : droneList) {
            new Thread(() -> {
                if (drone.getState() == State.DELIVERING)
                    drone.setState(State.DELIVERED);

                if (drone.getState() == State.LOADED)
                    drone.setState(State.DELIVERING);

                if (drone.getState() == State.DELIVERED)
                    drone.setState(State.RETURNING);

                if (drone.getState() == State.RETURNING)
                    drone.setState(State.IDLE);

                droneRepository.save(drone);
                logger.info(String.format("Drone state updated. Drone State: %s", drone.getState()));
            }).start();
        }
    }
}
