package com.musalasoft.dronedispatchcontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DroneDispatchControllerApplication {
	public static void main(String[] args) {
		SpringApplication.run(DroneDispatchControllerApplication.class, args);
	}
}
