package com.musalasoft.dronedispatchcontroller.controllers;

import com.musalasoft.dronedispatchcontroller.data.dto.drone.request.DroneRequest;
import com.musalasoft.dronedispatchcontroller.data.dto.medication.request.MedicationRequest;
import com.musalasoft.dronedispatchcontroller.services.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Define REST API functionalities
 */
@RestController
@RequestMapping(path = "/drones")
public class DroneController {
    @Autowired
    private DroneService droneService;

    @PostMapping
    public ResponseEntity<Object> registerDrone(@RequestBody DroneRequest drone) {
        return droneService.saveDrone(drone.getDrone());
    }

    @GetMapping
    public ResponseEntity<Object> getAllDrones() {
        return droneService.getAll();
    }

    @PostMapping(path = "/load/{id}")
    public ResponseEntity<Object> loadMedication(@RequestBody MedicationRequest medicationRequest,
                                                             @PathVariable("id") Long droneId) {
        return droneService.loadMedication(medicationRequest.getMedicationDto(), droneId);
    }

    @GetMapping(path = "/check/{id}")
    public ResponseEntity<Object> checkLoadedDrone(@PathVariable("id") Long droneId) {
        return droneService.getMedicationList(droneId);
    }

    @GetMapping(path = "/available")
    public ResponseEntity<Object> getAvailableDrones() {
        return droneService.getAvailableDrones();
    }

    @GetMapping(path = "/battery-level/{id}")
    public ResponseEntity<Object> checkBatteryLevel(@PathVariable("id") Long droneId) {
        return droneService.checkBatteryLevel(droneId);
    }

    @GetMapping(path = "/event-log/{id}")
    public ResponseEntity<Object> getEventLogById(@PathVariable("id") Long droneId) {
        return droneService.getEventLogByDroneId(droneId);
    }
}
