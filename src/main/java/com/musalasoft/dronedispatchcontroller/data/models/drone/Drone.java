package com.musalasoft.dronedispatchcontroller.data.models.drone;

import com.musalasoft.dronedispatchcontroller.data.models.medication.Medication;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Drone model, defines a model in database
 */
@Entity
public class Drone {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @Column(name = "serial_number", length = 100, nullable = false)
    private String serialNumber;
    @Column(nullable = false)
    private String model;
    @Column(nullable = false, name = "weight_limit")
    private int weightLimit;
    @Column(nullable = false, name = "battery_capacity")
    private int batteryCapacity = 100;
    @Column(nullable = false)
    private State state = State.IDLE;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "drone")
    private List<Medication> medicationList;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Medication> getMedicationList() {
        return medicationList;
    }

    public void setMedicationList(List<Medication> medicationList) {
        this.medicationList = medicationList;
    }

    public int getWeightLimit() {
        return weightLimit;
    }

    public void setWeightLimit(int weightLimit) {
        this.weightLimit = weightLimit;
    }
}
