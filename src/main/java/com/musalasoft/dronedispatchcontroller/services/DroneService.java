package com.musalasoft.dronedispatchcontroller.services;

import com.musalasoft.dronedispatchcontroller.data.dto.drone.request.DroneDto;
import com.musalasoft.dronedispatchcontroller.data.dto.drone.response.DroneResponse;
import com.musalasoft.dronedispatchcontroller.data.dto.events.EventResponse;
import com.musalasoft.dronedispatchcontroller.data.dto.medication.request.MedicationDto;
import com.musalasoft.dronedispatchcontroller.data.dto.medication.response.MedicationResponse;
import com.musalasoft.dronedispatchcontroller.data.dto.medication.response.MedicationsResponse;
import com.musalasoft.dronedispatchcontroller.data.dto.message.MessageResponse;
import com.musalasoft.dronedispatchcontroller.data.models.drone.Drone;
import com.musalasoft.dronedispatchcontroller.data.models.drone.DroneRepository;
import com.musalasoft.dronedispatchcontroller.data.models.drone.State;
import com.musalasoft.dronedispatchcontroller.data.models.eventlog.BatteryEventLog;
import com.musalasoft.dronedispatchcontroller.data.models.eventlog.BatteryEventLogRepository;
import com.musalasoft.dronedispatchcontroller.data.models.medication.Medication;
import com.musalasoft.dronedispatchcontroller.data.models.medication.MedicationRepository;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.musalasoft.dronedispatchcontroller.utils.Constants.modelList;

@Service
public class DroneService {
    @Autowired
    private MedicationRepository medicationRepository;

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private BatteryEventLogRepository eventLogRepository;

    @Autowired
    private Logger logger;

    public ResponseEntity<Object> saveDrone(DroneDto drone) {
        if (!modelList.containsKey(drone.getModel())) {
            return ResponseEntity.badRequest().body(new MessageResponse("The model specified is not allowed"));
        }

        Drone savedDrone = new Drone();
        savedDrone.setModel(drone.getModel());
        savedDrone.setWeightLimit(modelList.get(drone.getModel()));
        savedDrone.setSerialNumber(drone.getSerialNumber());
        savedDrone = droneRepository.save(savedDrone);

        logger.info("Drone created");

        DroneResponse dto = new DroneResponse();
        dto.setDrone(savedDrone);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    public ResponseEntity<Object> loadMedication(MedicationDto medicationDto, Long droneId) {
        if (medicationDto.getName() == null || medicationDto.getName().equals(""))
            return ResponseEntity.badRequest().body(new MessageResponse("Missing arguments"));

        if (medicationDto.getCode() == null || medicationDto.getCode().equals(""))
            return ResponseEntity.badRequest().body(new MessageResponse("Missing arguments"));

        if (medicationDto.getWeight() == 0)
            return ResponseEntity.badRequest().body(new MessageResponse("Missing arguments"));

        Drone drone = droneRepository.findById(droneId).orElse(null);
        if (drone == null) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse(String.format("Drone with ID: %d not found", droneId)));
        }

        if (drone.getState() == State.LOADING || drone.getState() == State.DELIVERING
                || drone.getState() == State.DELIVERED || drone.getState() == State.RETURNING) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse(String.format("Already loading drone ID %d", droneId)));
        }
        drone.setState(State.LOADING);
        droneRepository.save(drone);

        if (drone.getBatteryCapacity() <= 25) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(
                    new MessageResponse(String.format("Drone ID %d battery capacity is below 25",
                            droneId)));
        }

        Medication medication = new Medication();
        medication.setCode(medicationDto.getCode());
        medication.setName(medicationDto.getName());
        medication.setImageUrl(medicationDto.getImageUrl());
        medication.setWeight(medicationDto.getWeight());
        medication.setDrone(drone);

        int weightLimit = drone.getWeightLimit();
        int cumulativeWeight = 0;

        List<Medication> medicationList = drone.getMedicationList();
        if (medicationList.isEmpty()) {
            if (medication.getWeight() >= drone.getWeightLimit())
                return new ResponseEntity<>(new MessageResponse("Weight limit exceeded"), HttpStatus.BAD_REQUEST);
        } else {
            for (Medication item : medicationList) {
                cumulativeWeight += item.getWeight();
            }

            cumulativeWeight += medication.getWeight();

            if (cumulativeWeight >= weightLimit) {
                return new ResponseEntity<>(new MessageResponse("Weight limit exceeded"), HttpStatus.BAD_REQUEST);
            }
        }

        medicationRepository.save(medication);

        drone.setState(State.LOADED);
        droneRepository.save(drone);

        logger.info("Drone loaded with medication");

        MedicationResponse response = new MedicationResponse();
        response.setMedication(medication);
        return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
    }

    public ResponseEntity<Object> getMedicationList(Long droneId) {
        Drone drone = droneRepository.findById(droneId).orElse(null);

        if (drone != null) {
            MedicationsResponse response = new MedicationsResponse();
            response.setDrone(drone);
            return ResponseEntity.ok(response);
        } else {
            return ResponseEntity.badRequest().body(new MessageResponse("Drone was not found"));
        }
    }

    public ResponseEntity<Object> getAvailableDrones() {
        List<Drone> droneList = droneRepository.findByState(State.IDLE);
        droneList.addAll(droneRepository.findByState(State.LOADED));

        if (droneList.isEmpty()) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(droneList);
        }
    }

    public ResponseEntity<Object> checkBatteryLevel(Long droneId) {
        Drone drone = droneRepository.findById(droneId).orElse(null);

        if (drone != null) {
            return ResponseEntity.ok(new MessageResponse(String.format("Drone, ID %s, capacity is: %d", droneId, drone.getBatteryCapacity())));
        } else {
            return ResponseEntity.badRequest().body(new MessageResponse("Drone was not found"));
        }
    }

    public ResponseEntity<Object> getEventLogByDroneId(Long droneId) {
        List<BatteryEventLog> eventLogList = eventLogRepository.findByDroneId(droneId);

        if (eventLogList.isEmpty()) {
            return ResponseEntity.badRequest().body(new MessageResponse("Events not found"));
        }

        EventResponse response = new EventResponse();
        response.setEventLogList(eventLogList);

        return ResponseEntity.ok(response);
    }

    public ResponseEntity<Object> getAll() {
        return ResponseEntity.ok(droneRepository.findAll());
    }
}
